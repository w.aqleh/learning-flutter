import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  // this is a pointer to main._resetQuiz()
  // this way we can call the private method _resetQuiz in main
  final VoidCallback resetHandler;

  //constructor
  Result(this.resultScore, this.resetHandler);

  // this is a getter not a normal method, it does not have `()`
  String get resultPhrase {
    var resultText;
    if (resultScore <= 8) {
      resultText = 'you are awsome and innocent!';
    } else if (resultScore <= 12) {
      resultText = 'pretty likable';
    } else if (resultScore <= 12) {
      resultText = 'you are ... strange';
    } else {
      resultText = 'you are so bad';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue, // background
              onPrimary: Colors.white, // text
            ),
            onPressed: resetHandler,
            child: Text('Restart Quiz!'),
          ),
        ],
      ),
    );
  }
}
