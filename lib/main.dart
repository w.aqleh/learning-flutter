import 'package:flutter/material.dart';

import './quiz.dart';
import './answer.dart';
import './result.dart';

// void main() {
//   runApp(MyApp());
// }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

// leading underscore in class and method names declares them as private

// the leading underscore in the class name informs dart this MyAppState should
// only be used inside main.dart file
// it is a private class
class _MyAppState extends State<MyApp> {
  // create a questions and answers Map object
  final _questions = const [
    {
      'questionText': 'What\'s your favorite color?',
      'answers': [
        {'text': 'Black', 'score': 10},
        {'text': 'Red', 'score': 5},
        {'text': 'Green', 'score': 3},
        {'text': 'White', 'score': 1},
      ],
    },
    {
      'questionText': 'What\'s your favorite animal?',
      'answers': [
        {'text': 'Rabbit', 'score': 10},
        {'text': 'Snake', 'score': 5},
        {'text': 'Elephant', 'score': 3},
        {'text': 'Lion', 'score': 1},
      ],
    },
    {
      'questionText': 'Who\'s your favorite instructor?',
      'answers': [
        {'text': 'Max', 'score': 10},
        {'text': 'Max', 'score': 5},
        {'text': 'Max', 'score': 3},
        {'text': 'Max', 'score': 1},
      ],
    },
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    // setState forces flutter to rerender Widget build
    // it figures out what has changed and it will only rerender that
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    // setState forces flutter to rerender Widget build
    // it figures out what has changed and it will only rerender that
    setState(() {
      _questionIndex += 1;
    });
    print('answer 1 chosen!');
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    // create a list of questions
    // var questions = [
    //   'What\'s your favorite color?',
    //   'what\'s your favorite animal?',
    // ];

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        // if we have more questions load Column of Question and Answer(s)
        // else load Center with Text "You did it!"
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
