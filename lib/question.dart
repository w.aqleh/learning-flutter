import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  final String questionText;

  // constructor
  Question(this.questionText);

  @override
  Widget build(BuildContext context) {
    // used Container to center widgets in the screen, it is like a div
    return Container(
        width: double.infinity,
        // margin all sides by 10, use `.only` to target specific side
        // `.all` and `.only` are just constuctors EdgeInsets
        margin: EdgeInsets.all(10),
        child: Text(
          questionText,
          style: TextStyle(fontSize: 28),
          textAlign: TextAlign.center,
        ));
  }
}
