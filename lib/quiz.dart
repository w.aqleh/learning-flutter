import 'package:flutter/material.dart';

import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int questionIndex;
  // this is a pointer to main._answerQuestion()
  // this way we can call the private method _answerQuestion in main
  final Function answerQuestion;

  // constructor
  Quiz({
    required this.answerQuestion,
    required this.questions,
    required this.questionIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(
          questions[questionIndex]['questionText'].toString(),
        ),
        // pass a pointer to _answerQuestion
        // Answer(_answerQuestion),
        // Answer(_answerQuestion),
        // Answer(_answerQuestion),

        // List<Strings> tells dart the answers is a list of strings
        // loop the answers list and constuct multiple widget Answer objects
        // with _answerQuestion pointer and answer text.
        // the 3 dots pulls the items from the list and adds them to the
        // as siblings to Question widget
        ...(questions[questionIndex]['answers'] as List<Map<String, Object>>)
            // .map((answer) => Answer(answerQuestion, answer))
            // to be able to pass parameter to answerQuestion pointer we add () =>
            .map((answer) =>
                // Answer(() => answerQuestion(answer['score'], answer['text']),)
                Answer(() => answerQuestion(answer['score']),
                    answer['text'].toString()))
            .toList()
      ],
    );
  }
}
