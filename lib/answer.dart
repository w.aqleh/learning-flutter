import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  // this is a pointer to main._answerQuestion()
  // this way we can call the private method _answerQuestion in main
  final VoidCallback selectHandler;
  final String answerText;

  // constructor
  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue, // background
          onPrimary: Colors.white, // text
        ),
        onPressed: selectHandler,
        child: Text(answerText),
      ),
    );
  }
}
